import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RefDataService } from '../../../shared/services/refdata.service';
import { WorkflowStateService } from '../../../shared/services/workflowstate.service';
import { ProgramWorkflowState } from '../../../shared/models/ProgramWorkflowState';
import { FileWorkflowState } from '../../../shared/models/FileWorkflowState';

@Component({
    selector: 'app-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

    //current values for the drop down selectors
    cycles: String[];
    //currently selected values in the drop downs
    public cycle: String = 'IPBS21';

    //report information (populated with some default values until the report has calculated)
    fileTypeList: FileWorkflowState[] = [{ "fileTypeName": "Fiscal File", "fileTypeId": "F", "currentStates": [], "nextStates": [] }];
    workflowStateReport: ProgramWorkflowState[] = [{ "programName": "NA", "programId": "NA", "fileTypeInfo": this.fileTypeList }];

    constructor(private http: HttpClient,
        private refDataService: RefDataService,
        private workflowStateService: WorkflowStateService) { }

    ngOnInit() {
        //do an initial population of the drop downs
        this.getAvailableCycles();
        //do an initial population of the report.
        this.getWorkflowStateReport(this.cycle);
    }

    getAvailableCycles(): void {
        this.refDataService.getCycles()
            .subscribe(cycles => this.cycles = cycles);
    }

    getWorkflowStateReport(cycle: String): void {
        this.workflowStateService.getWorkflowStateSummaryReport(cycle)
            .subscribe(workflowStateReport => this.workflowStateReport =
                workflowStateReport);
        this.fileTypeList = this.workflowStateReport[0].fileTypeInfo;
    }

    /**
     * When user selects a new cycle, the report needs to be recalculated.
     * @param cycle the cycle/period (id not name) for the report
     */
    selectedCycle(cycle: string) {
        this.cycle = cycle;
        //refresh the report

        this.getWorkflowStateReport(cycle);
    }

}
