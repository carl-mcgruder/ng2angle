import { Component, OnInit } from '@angular/core';
import { FileUploader, FileItem } from 'ng2-file-upload';
import { timer } from 'rxjs';
import { switchMap, map, takeWhile, tap, finalize } from 'rxjs/operators';
import { FileType } from '../../../shared/models/FileType';
import { Program } from '../../../shared/models/Program';
import { Multiplier } from '../../../shared/models/Multiplier';
import { RefDataService } from '../../../shared/services/refdata.service';
import { ValidationStatusService } from '../../../shared/services/validationstatus.service';

/**
 * Component associated with file submission upload page.
 */
@Component({
    selector: 'app-upload',
    templateUrl: './upload.component.html',
    styleUrls: ['./upload.component.scss']
})
export class UploadComponent implements OnInit {

    baseURL = '/files/upload/'

    public transactionId = "";
    public completedMessageList = [];
    public completedMessageString = "";
    public progressPercent = 0;
    public cycle: String = 'IPBS21';
    public fileType: String = 'default';
    public program: String = 'default';
    public multiplier: number = 1000;

    //available choices for the drop downs on page
    cycles: String[];
    fileTypes: FileType[];
    programs: Program[];
    multipliers: Multiplier[];

    public uploader: FileUploader = new FileUploader({ url: `${this.baseURL}${this.cycle}/${this.fileType}` });

    public hasBaseDropZoneOver: boolean = false;
    public hasAnotherDropZoneOver: boolean = false;

    public fileOverBase(e: any): void {
        this.hasBaseDropZoneOver = e;
    }

    public fileOverAnother(e: any): void {
        this.hasAnotherDropZoneOver = e;
    }

    constructor(private refDataService: RefDataService, private validationStatusService: ValidationStatusService) { }

    ngOnInit() {
        this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
            this.transactionId = response.slice(1, -1);
            console.log(this.transactionId);
            timer(500, 250).pipe(switchMap(() =>
                this.validationStatusService.getPercentProgress(this.transactionId, this.cycle,
                    this.fileType, this.program)),
                map(value => value * 100),
                takeWhile(percentage => percentage < 100, true),
                finalize(() =>
                    this.validationStatusService.getValidationStatus(this.transactionId, this.cycle,
                        this.fileType, this.program).subscribe(
                            value => this.completedMessageList = value
                        )),
                tap(percentage => this.progressPercent = percentage)
            ).subscribe();
        };

        //make sure currently needed url is used on each upload
this.uploader.onBeforeUploadItem = (fileItem: FileItem): any => {
    // swap the URL to reflect correct cycle/filetype/program/multiplier with each upload
    fileItem.url = `${this.baseURL}${this.cycle}/${this.fileType}/${this.program}?multiplier=${this.multiplier}`;
    return {fileItem};
 };

        //initially populate the drop downs with something
        this.getAvailableCycles();
        this.getAvailableFileTypes(this.cycle);
        this.getAvailablePrograms(this.cycle);
        this.getAvailableMultipliers(this.cycle);
    }

    getAvailableCycles(): void {
        this.refDataService.getCycles()
            .subscribe(cycles => this.cycles = cycles);
    }

    getAvailableFileTypes(cycle: String): void {
        this.refDataService.getFileTypes(cycle)
            .subscribe(fileTypes => this.fileTypes = fileTypes)
    }

    getAvailablePrograms(cycle: String): void {
        this.refDataService.getPrograms(cycle)
            .subscribe(programs => this.programs = programs)
    }

    getAvailableMultipliers(cycle: String): void {
        this.refDataService.getMultipliers(cycle)
            .subscribe(multipliers => this.multipliers = multipliers);
    }

    /**
     * When user selects a new cycle, the drop downs all need to be recalculated.
     * @param cycle the cycle/period (id not name) for the file submission
     */
    selectedCycle(cycle: string) {
        this.cycle = cycle;
        //refresh the choices for drop down to current period
        this.getAvailableFileTypes(cycle);
        this.getAvailablePrograms(cycle);
        this.getAvailableMultipliers(cycle);
        console.debug("selected cycle: " + cycle)
    }

    selectedFileType(fileType: string) {
        this.fileType = fileType;
        console.debug("selected file type: " + fileType)
    }

    selectedProgram(program: string) {
        this.program = program;
        console.debug("selected program:" + program)
    }

    selectedMultiplier(multiplier: number) {
        this.multiplier = multiplier;
        console.debug("selected multiplier: " + multiplier)
    }

}