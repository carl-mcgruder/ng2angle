import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { FileType } from '../../../shared/models/FileType';
import { Program } from '../../../shared/models/Program';
import { Comment } from '../../../shared/models/Comment';
import { StateChange } from '../../../shared/models/StateChange';
import { WorkflowState } from '../../../shared/models/WorkflowState';
import { RefDataService } from '../../../shared/services/refdata.service';
import { CommentService } from '../../../shared/services/comment.service';
import { StateChangeService } from '../../../shared/services/statechange.service';
import { ValidationStatusService } from '../../../shared/services/validationstatus.service';
import { WorkflowStateService } from '../../../shared/services/workflowstate.service';
import { SubmissionService } from '../../../shared/services/submission.service';

/**
 * Component code for the Overall Status form. It allows user to pick cycle, file type and ID. Once done,
 * it returns the current workflow state (with next states available to be selected), the latest validation
 * status messages and an area to interact with comments regarding this file submission.
 */

@Component({
  selector: 'app-standard',
  templateUrl: './standard.component.html',
  styleUrls: ['./standard.component.scss']
})
export class StandardComponent implements OnInit {

  //currently selected values in the drop downs
  public cycle: String = 'IPBS21';
  public fileType: String = 'default';
  public program: String = 'default';

  //current values for the drop down selectors
  cycles: String[];
  fileTypes: FileType[];
  programs: Program[];

  //latest transaction's validation status messages for the file submission
  validationStatus: String[] = [];

  //comments regarding the file submission
  comments: Comment[] = [];

  //boolean whether to display the comment add section
  showAddComment: boolean = false;
  commentForm: FormGroup;

  //state changes regardign the file submission
  stateChanges: StateChange[] = [];

  //workflow state for the file submission
  currWorkflowStates: WorkflowState[] = [];
  nextWorkflowStates: WorkflowState[] = [];

  //downloadable file content
  submissionBlob: Blob;

  constructor(private http: HttpClient,
    private refDataService: RefDataService,
    private commentService: CommentService,
    private stateChangeService: StateChangeService,
    private validationStatusService: ValidationStatusService,
    private workflowStateService: WorkflowStateService,
    private submissionService: SubmissionService,
    private formBuilder: FormBuilder) {
    this.commentForm = this.formBuilder.group({
      comment: ''
    });
  }

  ngOnInit() {
    //do an initial population of the drop downs
    this.getAvailableCycles();
    this.getAvailableFileTypes(this.cycle);
    this.getAvailablePrograms(this.cycle);

    //subscribe to the observable that will show comments/validationstatus/workflow state
    this.commentService.comments
      .subscribe(comments => this.comments = comments);
    this.stateChangeService.stateChanges
      .subscribe(stateChanges => this.stateChanges = stateChanges);
    this.validationStatusService.statuses
      .subscribe(statuses => this.validationStatus = statuses);

  }

  getAvailableCycles(): void {
    this.refDataService.getCycles()
      .subscribe(cycles => this.cycles = cycles);
  }
  getAvailableFileTypes(cycle: String): void {
    this.refDataService.getFileTypes(cycle)
      .subscribe(fileTypes => this.fileTypes = fileTypes);
  }
  getAvailablePrograms(cycle: String): void {
    this.refDataService.getPrograms(cycle)
      .subscribe(programs => this.programs = programs);
  }

  getCurrentWorkflowStates(): void {
    this.workflowStateService.getCurrWorkflowStates(this.cycle, this.fileType,
      this.program)
      .subscribe(currWorkflowStates => this.currWorkflowStates =
        currWorkflowStates);
  }

  getNextWorkflowStates(): void {
    this.workflowStateService.getNextWorkflowStates(this.cycle, this.fileType,
      this.program)
      .subscribe(nextWorkflowStates => this.nextWorkflowStates =
        nextWorkflowStates);
  }

  /**
   * When user selects a new cycle, the drop downs, validation status, workflow
   * state and comments all need to be recalculated.
   * @param cycle the cycle/period (id not name) for the file submission
   */

  selectedCycle(cycle: string) {
    this.cycle = cycle;
    //refresh the choices for drop down to current period
    this.getAvailableFileTypes(cycle);
    this.getAvailablePrograms(cycle);
    this.commentService.loadAllComments(cycle, this.fileType, this.program);
    this.stateChangeService.loadAllStateChanges(cycle, this.fileType,
      this.program);
    this.validationStatusService.loadLatestStatus(cycle, this.fileType,
      this.program);
    this.getCurrentWorkflowStates();
    this.getNextWorkflowStates();
    console.debug("selected cycle: " + cycle)
  }

  /**
   * When user selects a new file type, the validation status, workflow state and
   * comments all need to be recalculated.
   * @param fileType the file type (id not name) for the file submission
   */

  selectedFileType(fileType: string) {
    this.fileType = fileType;
    this.commentService.loadAllComments(this.cycle, fileType, this.program);
    this.stateChangeService.loadAllStateChanges(this.cycle, fileType,
      this.program);
    this.validationStatusService.loadLatestStatus(this.cycle, fileType, this.program);
    this.getCurrentWorkflowStates();
    this.getNextWorkflowStates();
    console.debug("selected file type: " + fileType)
  }

  /**
   * When user selects a new program, the validation status, workflow state and
   * comments all need to be recalculated.
   * @param program the program (id not name) for the file submission
   */
  selectedProgram(program: string) {
    this.program = program;
    this.commentService.loadAllComments(this.cycle, this.fileType, program);
    this.stateChangeService.loadAllStateChanges(this.cycle, this.fileType, program);
    this.validationStatusService.loadLatestStatus(this.cycle, this.fileType, program);
    this.getCurrentWorkflowStates();
    this.getNextWorkflowStates();
    console.debug("selected program: " + program)
  }

  createComment(comment: Comment) {
    console.log("Comment: " + comment);
    //fix some data on the comment before sending it to service
    comment.fileTypeId = this.fileType;
    comment.programId = this.program;
    comment.periodId = this.cycle;
    //send comment to service to get it persisted
    this.commentService.createComment(comment);
    //reset things on form (i.e. clear comment field and hide the section)
    this.showAddComment = false;
    this.commentForm.reset();
  }

  updateComment(comment: Comment) {
    this.commentService.updateComment(comment);
  }

  retrieveSubmission(): void {
    this.submissionService.retrieveSubmission(this.cycle, this.fileType, this.program).subscribe((data) => {
      this.submissionBlob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      var downloadURL = window.URL.createObjectURL(data);
      var link = document.createElement('a');
      var nowString = new Date().toISOString();
      link.href = downloadURL;
      link.download = `submission_${this.cycle}_${this.fileType}_${this.program}_${nowString}.xlsx`;
      link.click();
    })
  }

  setWorkflowState(stateId: String) {
    //send request and reset current states
    this.workflowStateService.setWorkflowState(this.cycle, this.fileType, this.program, stateId)
      .subscribe(currWorkflowStates => {
      this.currWorkflowStates =
        currWorkflowStates; this.getNextWorkflowStates();
      });
  }

  showAddCommentSection() {
    this.showAddComment = true;
  }
  hideAddCommentSection() {
    this.showAddComment = false;
  }


}
