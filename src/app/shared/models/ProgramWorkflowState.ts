import { FileWorkflowState } from './FileWorkflowState';

export class ProgramWorkflowState {
    programName: string;
    programId: string;
    fileTypeInfo: FileWorkflowState[];
}