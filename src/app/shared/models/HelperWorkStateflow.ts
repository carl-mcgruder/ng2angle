import { Observable } from 'rxjs/Observable'
import { WorkflowState } from './WorkflowState';

export class HelperWorkflowState extends Observable<HelperWorkflowState>{
    currentStates: WorkflowState[];
    nextStates: WorkflowState[];
}