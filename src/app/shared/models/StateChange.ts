export class StateChange {
    stateChangeId: String
    stateId: String
    stateName: String
    subStateName: String
    display: String
    programId: String
    periodId: String
    fileType: String
    userld: String
    usernameDisplay: String
    createdTimestampString: String
}