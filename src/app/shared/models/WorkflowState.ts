import { Observable } from 'rxjs/Observable'

export class WorkflowState extends Observable<WorkflowState> {
    stateId: String;
    stateName: String;
    orderNumber: Number;
    stateDescription: String;
    subStateName: String;
    subStateDescription: String;
    subStateCount: Number;
    periodId: String;
    // userTypeName: String;
    nextStateOrder: Number;
    programId: String;
    programName: String;
    fileTypeId: String;
    fileTypeName: String;
    // updatedBy: String;
    // updatedDate: Date;
    nextStates: WorkflowState[];
}