export class Comment {
    id: String
    comment: String
    programId: String
    periodId: String
    fileTypeId: String
    active: boolean
    createdDate: String
    createdBy: String
    usernameDisplay: String
    updatedDate: String
    updatedBy: String
}