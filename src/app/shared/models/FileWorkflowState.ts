import { WorkflowState } from './WorkflowState';

export class FileWorkflowState {
    fileTypeName: string;
    fileTypeId: string;
    currentStates: WorkflowState[];
    nextStates: WorkflowState[];
}