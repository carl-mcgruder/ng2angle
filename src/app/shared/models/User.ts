export class User {
    username: String
    authorities: String
    userType: String
    usernameDisplay: String
}