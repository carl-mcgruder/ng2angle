import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { StateChange } from '../models/StateChange';
import { MessageService } from './message.service';

//Service for interacting with State Change services in Auth Service
@Injectable()
export class StateChangeService {
    private _stateChanges = new BehaviorSubject<StateChange[]>([]);
    private dataStore: { stateChanges: StateChange[] } = { stateChanges: [] }; // store our data in memory
    readonly stateChanges = this._stateChanges.asObservable();

    constructor(private http: HttpClient, private messageService: MessageService) { }

    /**
     * Retrieve the list of state changes that match cycle, filetype and program id
     * @param cycle cycle/period the state changes are associated with (id not name)
     * @param fileType file type the state changes are associated with (id not name) 
     * @param programId program the state changes are associated with (id not name)
     * @returns returns the list of state changes
     */

    loadAllStateChanges(cycle: String, fileType: String, programId: String) {
        this.http.get(`/workflowstatechanges/${cycle}/${fileType}/${programId}`).subscribe(data => {
            this.dataStore.stateChanges = data as StateChange[];
            this._stateChanges.next(Object.assign({}, this.dataStore).stateChanges);
        }, error => catchError(this.handleError('loadAllStateChanges', [])));
    }
    
    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            console.error(error); //send error to console
            this.log(operation + ' failed because ' + error.message); //log error to screen
            return of(result as T); //send back a response of a type that won't crash app
        }
    }

    private log(message: String) {
        this.messageService.add('RefDataService: ' + message);
    }

}