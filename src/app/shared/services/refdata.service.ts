import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap, take } from 'rxjs/operators';
import 'rxjs/add/operator/map';
import { of } from 'rxjs/observable/of';
import { FileType } from '../models/FileType';
import { Program } from '../models/Program';
import { Multiplier } from '../models/Multiplier';
import { MessageService } from './message.service';
import { environment } from '../../../environments/environment';


//Service for looking up basic unauthenticated lookup data (i.e. file types and cycles). Goes through
// API Gateway to get data.
@Injectable()
export class RefDataService {

    private baseAPIGatewayUrl = environment.apiGatewayUrl;
    constructor(private http: HttpClient, private messageService: MessageService) { }

    getCycles(): Observable<String[]> {
        return this.http.get<String[]>(`${this.baseAPIGatewayUrl}/availableperiods`)
            .map((data: any) => {
                return data.periods as String[];
            })
            .pipe(
                catchError(this.handleError('getCycles', []))
            );
    }

    getFileTypes(cycle: String): Observable<FileType[]> {
        return this.http.get<FileType[]>(`${this.baseAPIGatewayUrl}/availablefiletypes/${cycle}`)
            .map((data: any) => {
                return data as FileType[];
            })
            .pipe(
                catchError(this.handleError('getFileTypes', []))
            );
    }

    getPrograms(cycle: String): Observable<Program[]> {
        return this.http.get<Program[]>(`${this.baseAPIGatewayUrl}/availableprograms/$(cycle}`)
            .map((data: any) => {
                return data as Program[];
            })
            .pipe(
                catchError(this.handleError('getPrograms', []))
            );
    }

    getMultipliers(cycle: String): Observable<Multiplier[]> {
        return this.http.get<Multiplier[]>(`${this.baseAPIGatewayUrl}/availablemultipliers/${cycle}`)
        .map((data: any) => {
            return data as Multiplier[];
        })
        .pipe(
            catchError(this.handleError('getMultipliers', []))
        );

    }
        
    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            console.error(error); //send error to console
            this.log(operation + ' failed because ' + error.message); //log error to screen
            return of(result as T); //send back a response of a type that won't crash app
        }
    }

    private log(message: String) {
        this.messageService.add('RefDataService: ' + message);
    }
}