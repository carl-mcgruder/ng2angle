import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import 'rxjs/add/operator/map';
import { of } from 'rxjs/observable/of';
import { WorkflowState } from '../models/WorkflowState';
import { ProgramWorkflowState } from '../models/ProgramWorkflowState';
import { MessageService } from './message.service';

//Service for interacting with Validation Status services in Auth Service.
@Injectable()
export class WorkflowStateService {
    constructor(private http: HttpClient, private messageService: MessageService) { }

    /**
     * Retrieve the current workflow state(s) of submission to match cycle, filetype and program id
     * @param cycle cycle/period the submission is associated with (id not name)
     * @param fileType file type the submission is associated with (id not name)
     * @param programId program the submission is associated with (id not name)
     */
    getCurrWorkflowStates(cycle: String, fileType: String, programId: String):
        Observable<WorkflowState[]> {
        return this.http.get<WorkflowState[]>(`/workflowstate/${cycle}/${fileType}/${programId}`)
            .map((data: any) => {
                return data as WorkflowState[];
            })
            .pipe(
                catchError(this.handleError('getCurrWorkflowStates', []))
            );
    }

    /**
     * Retrieve the next workflow state(s) of submission to match cycle, filetype and program id
     * @param cycle cycle/period the submission is associated with (id not name)
     * @param fileType file type the submission is associated with (id not name)
     * @param programId program the submission is associated with (id not name)
     */
    getNextWorkflowStates(cycle: String, fileType: String, programId: String):
        Observable<WorkflowState[]> {
        return this.http.get<WorkflowState[]>(`/workflowstate/next/${cycle}/${fileType}/${programId}`)
            .map((data: any) => {
                return data as WorkflowState[];
            })
            .pipe(
                catchError(this.handleError('getNextWorkflowStates', []))
            );
    }

    /**
     * Set a new workflow state on a submission to match cycle, filetype and program id.
     * @param cycle cycle/period the submission is associated with (id not name)
     * @param fileType file type the submission is associated with (id not name)
     * @param programId program the submission is associated with (id not name)
     * @param stateId id of new state to set
     */
    setWorkflowState(cycle: String, fileType: String, programId: String, stateId:
        String): Observable<WorkflowState[]> {
        return this.http.get<WorkflowState[]>(`/workflowstate/${cycle}/${fileType}/${programId}/${stateId}`)
            .map((data: any) => {
                return data as WorkflowState[];
            })
            .pipe(
                catchError(this.handleError('getNextWorkflowStates', []))
            )
    }

    /**
     * Retrieve a summary report of cross-file, cross-program workflow states (service binds list of programs to ones user has roles for).
     * @param cycle cycle/period the submission is associated with (id not name)
     */
    getWorkflowStateSummaryReport(cycle: String): Observable<ProgramWorkflowState[]> {
        return this.http.get<ProgramWorkflowState[]>(`/workflowstate/report/${cycle}`)
            .map((data: any) => {
                return data as ProgramWorkflowState[];
            })
            .pipe(
                catchError(this.handleError('getWorkflowStateSummaryReport', []))
            )
    }


    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            console.error(error); //send error to console
            this.log(operation + ' failed because ' + error.message); //log error to screen
            return of(result as T); //send back a response of a type that won't crash app
        }
    }

    private log(message: String) {
        this.messageService.add('RefDataService: ' + message);
    }

}