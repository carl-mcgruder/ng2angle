import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import {catchError,map,tap,take} from 'rxjs/operators';
import 'rxjs/add/operator/map';
import { of } from 'rxjs/observable/of';
import { User } from '../models/User';
import { MessageService } from './message.service';


//Service for looking up basic user data
@Injectable()
export class UserService {

    constructor(private http: HttpClient, private messageService: MessageService) {}

    currentUser: User;

    getWhoAmI(): Observable<User> {
        return this.http.get<User>(`/utilities/whoami`);
    }

    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            console.error(error); //send error to console
            this.log(operation + ' failed because ' + error.message); //log error to screen
            return of(result as T); //send back a response of a type that won't crash app
        }
    }

    private log(message: String) {
        this.messageService.add('RefDataService: ' + message);
    }


}