import { Injectable } from '@angular/core';
//Simple service used to keep track of various messages we want to display in bulk in UI (i.e. Errors)

@Injectable()
export class MessageService {

    messages: String[] = [];
    add(message: String) {
        this.messages.push(message);
    }
    clear() {
        this.messages = [];
    }
}