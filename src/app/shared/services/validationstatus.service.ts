import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { MessageService } from './message.service';

//Service for interacting with Validation Status services in Auth Service.
@Injectable()
export class ValidationStatusService {
    private _statuses = new BehaviorSubject<String[]>([]);
    private dataStore: { statuses: String[] } = { statuses: [] }; // store our data in memory
    readonly statuses = this._statuses.asObservable();

    constructor(private http: HttpClient, private messageService: MessageService) { }

    /**
     * Retrieve the list of statuses from latest transaction that match cycle, filetype and program id
     * @param cycle cycle/period the statuses are associated with (id not name)
     * @param fileType file type the statuses are associated with (id not name)
     * @param programId program the statuses are associated with (id not name)
     * @returns returns the list of statuses
     */
    loadLatestStatus(cycle: String, fileType: String, programId: String) {
        this.http.get(`/validation/status/${cycle}/${fileType}}/${programId}`).subscribe(data => {
            this.dataStore.statuses = data as String[];
            this._statuses.next(Object.assign({}, this.dataStore).statuses);
        }, error => catchError(this.handleError('loadLatestStatus', [])));
    }

    /**
     * Retrieve the percentage complete (of validations performed) from the transaction id that matches,
     * together with cycle, filetype and program
     * @param id the id of the stransaction to retrieve
     * @param cycle cycle/period the statuses are associated with (id not name)
     * @param fileType file type the statuses are associated with (id not name)
     * @param program program the statuses are associated with (id not name)
     */
    getPercentProgress(id: string, cycle: String, fileType: String, program: String):
        Observable<number> {
        return this.http.get<number>(
            `/validation/progress/${cycle}/${fileType}/${program}/${id}`
        );
    }

    /**
     * Retrieve the list of statuses from the transaction id that matches, together with cycle, filetype
     * and program
     * @param id the id of the stransaction to retrieve
     * @param cycle cycle/period the statuses are associated with (id not name)
     * @param fileType file type the statuses are associated with (id not name)
     * @param program program the statuses are associated with (id not name)
     */
    getValidationStatus(id: string, cycle: String, fileType: String, program: String):
        Observable<string[]> {
        return this.http.get<string[]>(`/validation/status/${cycle}/${fileType}/${program}/${id}`
        );
    }

    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            console.error(error); //send error to console
            this.log(operation + ' failed because ' + error.message); //log error to screen
            return of(result as T); //send back a response of a type that won't crash app
        }
    }

    private log(message: String) {
        this.messageService.add('RefDataService: ' + message);
    }
}