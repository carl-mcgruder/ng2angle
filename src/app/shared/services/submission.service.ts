import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { MessageService } from './message.service';

//Service for interacting with Submission as a File (i.e. downloading) services in Auth Service.
@Injectable()
export class SubmissionService {
    constructor(private http: HttpClient, private messageService: MessageService) { }

    /**
     * Retrieves the file of the submission (calculated from db).
     * @param cycle cycle/period the submission is associated with (id not name)
     * @param fileType file type the submission is associated with (id not name)
     * @param programId program the submission is associated with (id not name)
     */

    public retrieveSubmission(cycle: String, fileType: String, program: String):
        Observable<Blob> {
        return this.http.get(`/files/download/S{cycle}/${fileType}/${program}`, {
            responseType: 'blob'
        })
    }

    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            console.error(error); //send error to console
            this.log(operation + ' failed because ' + error.message); //log error to screen
            return of(result as T); //send back a response of a type that won't crash app
        }
    }

    private log(message: String) {
        this.messageService.add('RefDataService: ' + message);
    }
}