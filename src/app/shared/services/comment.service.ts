import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { Comment } from '../models/Comment';
import { MessageService } from './message.service';

//Service for interacting with Comments services in Auth Service.
@Injectable()
export class CommentService {

    private _comments = new BehaviorSubject<Comment[]>([]);
    readonly comments = this._comments.asObservable();
    private dataStore: { comments: Comment[] } = { comments: [] } ; // store our data in memory
    constructor(private http: HttpClient, private messageService: MessageService) { } 

    // getcomments() {
    //  return this.comments.asObservable();
    // }

    /**
     * Retrieve the list of comments that match cycle, filetype and program id
     * @param cycle cycle/period the comments are associated with (id not name)
     * @param fileType file type the comments are associated with (id not name)
     * @param programed program the comments are associated with (id not name)
     * @returns returns the list of comments
     */
     loadAllComments(cycle: String, fileType: String, programId: String) {
        this.http.get(`/comment/${cycle}/${fileType}/${programId}`).subscribe (data => {
            this.dataStore.comments = data as Comment[];
            this._comments.next(Object.assign({}, this.dataStore).comments);
        }, error => catchError(this.handleError('loadAllComments', [])));
      }

    /**
     * Create a new comment.
     * @param comment the comment object with data to create comment
     * @returns returns the created comment
     */
    createComment(comment: Comment) {
        this.http.post<Comment>(`/comment`, comment).subscribe(data => {
            this.dataStore.comments.push(data);
            this._comments.next(Object.assign({}, this.dataStore).comments);
        }, error => catchError(this.handleError('createComment', [])));
    }

    /**
     * Update an existing comment.
     * @param comment the comment object with data to update comment
     * @returns returns the updated comment
     */
    updateComment(comment: Comment) {
        this.http.put<Comment>(`/comment/` + comment.id, comment).subscribe(data => {
            this.dataStore.comments.push(data);
            this._comments.next(Object.assign({}, this.dataStore).comments);
        }, error => catchError(this.handleError('createComment', [])));
    }

    private handleError<T> (operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
        console.error(error); //send error to console
        this.log(operation + ' failed because ' + error.message); //log error to screen
        return of (result as T); //send back a response of a type that won't crash app
        }
    }

    private log(arg0: string) {
        throw new Error("Method not implemented.");
    }
}